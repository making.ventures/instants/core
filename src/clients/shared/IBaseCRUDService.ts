import {ECrudMethods, TDelete, TCreate, TGet, IBaseEntity, TList, IBaseSort, TListRequest, TUpdate} from '../../types';

export interface IBaseCRUDService<T extends IBaseEntity<number> = any> {
  [ECrudMethods.Get]: TGet<T>;
  [ECrudMethods.Create]: TCreate<T, T>;
  [ECrudMethods.Delete]: TDelete;
  [ECrudMethods.ListWithPageInfo]: TList<
    TListRequest<T> & IBaseSort<keyof T>,
    T
  >;
  [ECrudMethods.Update]: TUpdate<T, T>;
}
