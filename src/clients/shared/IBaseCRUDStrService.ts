import {TUpdate, ECrudMethods, TListRequest, IBaseEntity, TDeleteStr, TCreate, TGetStr, TList, IBaseSort} from '../../types';

export interface IBaseCRUDStrService<T extends IBaseEntity<string> = any> {
  [ECrudMethods.Get]: TGetStr<T>;
  [ECrudMethods.Create]: TCreate<T, T>;
  [ECrudMethods.Delete]: TDeleteStr;
  [ECrudMethods.ListWithPageInfo]: TList<
    TListRequest<T> & IBaseSort<keyof T>,
    T
  >;
  [ECrudMethods.Update]: TUpdate<T, T>;
}
