import {IBaseCRUDService, IBaseUIReprService} from '../shared';
import {IBaseEntity} from '../../types';

export interface IBaseService<T extends IBaseEntity<number>>
    extends IBaseUIReprService<T>,
    IBaseCRUDService<T> { }
