import {IBaseCRUDStrService, IBaseUIReprStrService} from '../shared';
import {IBaseEntity} from '../../types';

export interface IBaseStrService<T extends IBaseEntity<string>>
  extends IBaseUIReprStrService<T>,
  IBaseCRUDStrService<T> { }
