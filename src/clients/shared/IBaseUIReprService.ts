import {
  EReprMethods,
  IBaseEntity,
  IUIRepresentation,
  TList,
  IBaseSort,
  TListRequest,
  TGet,
} from '../../types';

export interface IBaseUIReprService<T extends IBaseEntity<number> = any> {
  [EReprMethods.ReprGet]: TGet<IUIRepresentation<number>>;
  [EReprMethods.ReprList]: TList<
    TListRequest<T> & IBaseSort<keyof T>,
    IUIRepresentation<number>
  >;
}
