import {EReprMethods, IBaseEntity, IUIRepresentation, TGetStr, TList, IBaseSort, TListRequest} from '../../types';

export interface IBaseUIReprStrService<T extends IBaseEntity<string> = any> {
  [EReprMethods.ReprGet]: TGetStr<IUIRepresentation<string>>;
  [EReprMethods.ReprList]: TList<
    TListRequest<T> & IBaseSort<keyof T>,
    IUIRepresentation<string>
  >;
}
