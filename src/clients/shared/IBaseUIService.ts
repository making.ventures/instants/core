import {IBaseEntity, IListRequest, TGet, TCreate, TList, TUpdate} from '../../types';
import {IBaseService} from './IBaseService';

export interface IBaseUIService<
  T extends IBaseEntity<number>,
  TUIGet extends IBaseEntity<number>,
  TUIUpdate,
  TUICreate,
  TUIForList,
  TUIForListRequest extends IListRequest
  > extends IBaseService<T> {
  uiGet: TGet<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<TUIForListRequest, TUIForList>;
  uiList: TList<TUIForListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
