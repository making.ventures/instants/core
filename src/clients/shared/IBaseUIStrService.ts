import {IBaseEntity, IListRequest, TCreate, TList, TUpdate, TGetStr} from '../../types';
import {IBaseStrService} from './IBaseStrService';

export interface IBaseUIStrService<
  T extends IBaseEntity<string>,
  TUIGet extends IBaseEntity<string>,
  TUIUpdate,
  TUICreate,
  TUIForList,
  TUIForListRequest extends IListRequest
  > extends IBaseStrService<T> {
  uiGet: TGetStr<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<TUIForListRequest, TUIForList>;
  uiList: TList<TUIForListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
