import {IBaseUIReprService} from './IBaseUIReprService';
import {IBaseEntity, TGet, TCreate, ILangListRequest, TUpdate, TList} from '../../types';

export interface IUILangService<
  TUIGet extends IBaseEntity<number>,
  TUIUpdate,
  TUICreate,
  TUIForList
  > extends IBaseUIReprService {
  uiGet: TGet<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<ILangListRequest, TUIForList>;
  uiList: TList<ILangListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
