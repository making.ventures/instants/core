import {IBaseUIReprStrService} from './IBaseUIReprStrService';
import {IBaseEntity, TCreate, ILangListRequest, TUpdate, TList, TGetStr} from '../../types';

export interface IUILangStrService<
  TUIGet extends IBaseEntity<string>,
  TUIUpdate,
  TUICreate,
  TUIForList
  > extends IBaseUIReprStrService {
  uiGet: TGetStr<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<ILangListRequest, TUIForList>;
  uiList: TList<ILangListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
