import {IBaseEntity, IListRequest, TGet, TCreate, TList, TUpdate} from '../../types';
import {IBaseUIReprService} from './IBaseUIReprService';

export interface IUIService<
  TUIGet extends IBaseEntity<number>,
  TUIUpdate,
  TUICreate,
  TUIForList,
  TUIForListRequest extends IListRequest
  > extends IBaseUIReprService {
  uiGet: TGet<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<TUIForListRequest, TUIForList>;
  uiList: TList<TUIForListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
