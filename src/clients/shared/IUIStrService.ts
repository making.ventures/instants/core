import {IBaseEntity, IListRequest, TGetStr, TCreate, TUpdate, TList} from '../../types';
import {IBaseUIReprStrService} from './IBaseUIReprStrService';

export interface IUIStrService<
  TUIGet extends IBaseEntity<string>,
  TUIUpdate,
  TUICreate,
  TUIForList,
  TUIForListRequest extends IListRequest
  > extends IBaseUIReprStrService<TUIGet> {
  uiGet: TGetStr<TUIGet>;
  uiCreate: TCreate<TUICreate, TUIGet>;

  // uiList: TList<TUIForListRequest, TUIForList>;
  uiList: TList<TUIForListRequest, TUIForList>;
  uiUpdate: TUpdate<TUIUpdate, TUIGet>;
}
