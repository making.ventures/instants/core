import {IBaseService} from './IBaseService';
import {IBaseStrService} from './IBaseStrService';

export type TClient = {
  getAuthId(): string;
  setAuthId(authId: string): void;
};

export type TEntityServices = {
  [id: string]: IBaseService<any> | IBaseStrService<any>;
};
