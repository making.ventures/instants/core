import {UrlObject} from "url";

export interface Route {
  url: string;
  as?: string;
}

export type TRoute = Route | UrlObject;

export interface IEntityRoutes {
  defaultList: TRoute;
  defaultView: (id: string | number) => TRoute;
  defaultCreate: TRoute;
  defaultEdit: (id: string | number) => TRoute;
}
