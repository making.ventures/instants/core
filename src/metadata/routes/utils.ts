import {IEntityRoutes} from './types';

export const defaultServiceRoutes: (serviceName: string) => IEntityRoutes = (
  serviceName: string,
) => ({
  defaultCreate: {
    as: `/services/${serviceName}/${'propsid'}`,
    url: '/services/[service]/[id]',
  },
  defaultEdit: (id: string | number) => ({
    as: `/services/${serviceName}/${id}`,
    url: '/services/[service]/[id]',
  }),
  defaultList: {
    as: `/services/${serviceName}/${'propsid'}`,
    url: '/services/[service]/[id]',
  },
  defaultView: (id: string | number) => ({
    as: `/services/${serviceName}/${id}`,
    url: '/services/[service]/[id]',
  }),
});
