import {ECrudMethods, EReprMethods} from '../types';
import {IMethodMetaCreate} from './types/method';
import {TModel} from './types/model';
import {baseModels, coreModels} from '../schemas/models';

export const crudMethodsMeta: (
  model: TModel,
) => Record<ECrudMethods, IMethodMetaCreate> = (model: TModel) => ({
  [ECrudMethods.Create]: {
    argumentTypes: [model],
    name: ECrudMethods.Create,
    returnType: model,
  },
  [ECrudMethods.Delete]: {
    argumentTypes: [baseModels.number],
    name: ECrudMethods.Delete,
    returnType: baseModels.number,
  },
  [ECrudMethods.Get]: {
    argumentTypes: [baseModels.number],
    name: ECrudMethods.Get,
    returnType: model,
  },
  [ECrudMethods.ListWithPageInfo]: {
    argumentTypes: [coreModels.IListRequest],
    name: ECrudMethods.ListWithPageInfo,
    returnType: model,
  },
  [ECrudMethods.Update]: {
    argumentTypes: [model],
    name: ECrudMethods.Update,
    returnType: model,
  },
});

export const reprMethodsMeta: Record<EReprMethods, IMethodMetaCreate> = {
  [EReprMethods.ReprGet]: {
    argumentTypes: [baseModels.number],
    name: EReprMethods.ReprGet,
    returnType: coreModels.IUIRepresentationNumberId,
  },
  [EReprMethods.ReprList]: {
    argumentTypes: [coreModels.IListRequest],
    name: EReprMethods.ReprList,
    returnType: coreModels.IUIRepresentationNumberId,
  },
};
