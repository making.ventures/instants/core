import {TServiceMeta} from './service';

export type TClientMeta<TClient> = {
  [P in keyof TClient]: TServiceMeta<TClient[P], keyof TClient[P]>;
};
