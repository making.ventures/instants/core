import {IHttpPath} from '../../types';
import {TModel} from './model';

export interface IMethodMetaCreate
  extends Omit<IMethodMeta, 'description' | 'public' | 'fullName'> {
  public?: boolean; // Should be exposed without authentication
  description?: string;
}

export interface IMethodMeta {
  argumentTypes: TModel[];
  deprecated?: boolean; // will be deleted in future
  description: string;
  fullName: string;
  httpPath?: IHttpPath;
  internal?: boolean; // not part of public contract
  name: string;
  public: boolean; // Should be exposed without authentication
  returnType?: TModel;
  service?: boolean; // for special use
}

export type TServiceMethod<TService> = keyof TService;
