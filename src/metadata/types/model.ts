import {TFieldScheme} from '../../scheme/types';

export type TModel = Record<string, TFieldScheme> | TFieldScheme;
