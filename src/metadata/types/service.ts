import {TServiceMethod, IMethodMeta, IMethodMetaCreate} from './method';
import {TModel} from './model';

export enum EServiceType {
  Custom = 'Custom',
  Repr = 'Repr',
  Catalog = 'Catalog',
}

export interface IServiceMeta<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > {
  name: string;
  deprecated?: boolean; // will be deleted in future
  methods: Record<TMethodMetaName, IMethodMeta>;
  type: EServiceType;
  public: boolean; // Should be exposed without authentication
  description: string;
}

export interface IServiceMetaCreate<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  >
  extends Omit<
  IServiceMeta<TService, TMethodMetaName>,
  'methods' | 'public' | 'description'
  > {
  methods: Record<TMethodMetaName, IMethodMetaCreate>;
  public?: boolean; // Should be exposed without authentication
  description?: string;
}

export type TServiceMeta<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > =
  | IServiceCatalogMeta<TService, TMethodMetaName>
  | IServiceCustomMeta<TService, TMethodMetaName>
  | IServiceReprMeta<TService, TMethodMetaName>;

// Catalog

export interface IServiceCatalogMetaCreate<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMetaCreate<TService, TMethodMetaName> {
  mainTypeSchema: TModel;
  type: EServiceType.Catalog;
  explicitId?: boolean;
  stringId?: boolean;
}

export interface IServiceCatalogMeta<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMeta<TService, TMethodMetaName> {
  mainTypeSchema: TModel;
  type: EServiceType.Catalog;
  explicitId: boolean; // Whether id should be seted explisitly
  stringId?: boolean;
}

// Custom

export interface IServiceCustomMetaCreate<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMetaCreate<TService, TMethodMetaName> {
  type: EServiceType.Custom;
}

export interface IServiceCustomMeta<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMeta<TService, TMethodMetaName> {
  type: EServiceType.Custom;
}

// Repr

export interface IServiceReprMetaCreate<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMetaCreate<TService, TMethodMetaName> {
  type: EServiceType.Custom;
  stringId?: boolean;
}

export interface IServiceReprMeta<
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
  > extends IServiceMeta<TService, TMethodMetaName> {
  type: EServiceType.Repr;
  stringId?: boolean;
}
