import {fromPairs, toPairs, flatten} from 'lodash';
import {IMethodMeta, TServiceMethod, IMethodMetaCreate} from './types/method';
import {
  IServiceCatalogMetaCreate,
  IServiceCatalogMeta,
  IServiceCustomMetaCreate,
  IServiceReprMetaCreate,
  IServiceReprMeta,
  IServiceCustomMeta,
  TServiceMeta,
  EServiceType,
} from './types/service';
import {TClientMeta} from './types/client';
import {ECrudMethods, EReprMethods} from '../types';
import {crudMethodsMeta, reprMethodsMeta} from './shared';

export const fullMethodName = <
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
>(
    serviceName: string,
    meta: Record<TMethodMetaName, IMethodMetaCreate>,
  ): Record<TMethodMetaName, IMethodMeta> =>
  fromPairs(
    toPairs(meta).map(([name, methodMeta]) => [
      name,
      {
        ...(methodMeta as IMethodMeta),
        fullName: `${serviceName}.${(methodMeta as IMethodMeta).name}`,
      },
    ]),
  ) as Record<TMethodMetaName, IMethodMeta>;

export const crudMeta = <TService>(
  meta: Omit<
    IServiceCatalogMetaCreate<
      TService,
      TServiceMethod<Omit<TService, ECrudMethods | EReprMethods>>
    >,
    'type'
  >,
): IServiceCatalogMeta<TService> => {
  const methods = ({
    ...crudMethodsMeta(meta.mainTypeSchema),
    ...reprMethodsMeta,
    ...meta.methods,
  } as unknown) as Record<keyof TService, IMethodMeta>;

  return {
    ...meta,
    deprecated: meta.deprecated || false,
    description: meta.description || '',
    explicitId: meta.explicitId || false,
    methods: fullMethodName(meta.name, methods),
    public: meta.public || false,
    stringId: meta.stringId || false,
    type: EServiceType.Catalog,
  };
};

export const plainMeta = <
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
>(
    meta: Omit<IServiceCustomMetaCreate<TService, TMethodMetaName>, 'type'>,
  ): IServiceCustomMeta<TService, TMethodMetaName> => ({
    ...meta,
    deprecated: meta.deprecated || false,
    description: meta.description || '',
    methods: fullMethodName(meta.name, meta.methods),
    public: meta.public || false,
    type: EServiceType.Custom,
  });

export const reprMeta = <
  TService,
  TMethodMetaName extends keyof any = TServiceMethod<TService>
>(
    meta: Omit<IServiceReprMetaCreate<TService, TMethodMetaName>, 'type'>,
  ): IServiceReprMeta<TService, TMethodMetaName> => {
  const methods = ({
    ...reprMethodsMeta,
    ...meta.methods,
  } as unknown) as Record<TMethodMetaName, IMethodMetaCreate>;

  return {
    ...meta,
    deprecated: meta.deprecated || false,
    description: meta.description || '',
    methods: fullMethodName(meta.name, methods),
    public: meta.public || false,
    stringId: meta.stringId || false,
    type: EServiceType.Repr,
  };
};

export const crudMeta2 = <TService>(
  meta: Omit<
    IServiceCatalogMetaCreate<
      TService,
      TServiceMethod<Omit<TService, ECrudMethods | EReprMethods>>
    >,
    'type'
  >,
): IServiceCatalogMeta<TService> => {
  const methods = ({
    ...crudMethodsMeta(meta.mainTypeSchema),
    ...reprMethodsMeta,
    ...meta.methods,
  } as unknown) as Record<keyof TService, IMethodMeta>;

  return {
    ...meta,
    deprecated: meta.deprecated || false,
    description: meta.description || '',
    explicitId: meta.explicitId || false,
    methods: fullMethodName(meta.name, methods),
    public: meta.public || false,
    stringId: meta.stringId || false,
    type: EServiceType.Catalog,
  };
};

export const toMethodsMeta = <TClient>(
  clientMeta: TClientMeta<TClient>,
): Record<string, IMethodMeta> => {
  const joint = toPairs(clientMeta).map(([, serviceMeta]) => {
    const methods = (serviceMeta as TServiceMeta<any>).methods;

    return toPairs(methods).map(([, methodMeta]) => [
      methodMeta.fullName,
      methodMeta,
    ]);
  });

  const flat = flatten(joint);

  return fromPairs(flat);
};
