import ICurrency from './models/ICurrency.json';
import IGeo from './models/IGeo.json';
import ILanguage from './models/ILanguage.json';
import IListRequest from './models/IListRequest.json';
import IUIRepresentationNumberId from './models/IUIRepresentationNumberId.json';
import IUIRepresentationStringId from './models/IUIRepresentationStringId.json';
import baseString from './base/string.json';
import baseNumber from './base/number.json';
import baseBoolean from './base/boolean.json';
import {TFieldScheme} from '../scheme/types';

export const coreModels: Record<string, Record<any, TFieldScheme>> = {
  ICurrency: ICurrency as Record<any, TFieldScheme>,
  IGeo: IGeo as Record<any, TFieldScheme>,
  ILanguage: ILanguage as Record<any, TFieldScheme>,
  IListRequest: IListRequest as Record<any, TFieldScheme>,
  IUIRepresentationNumberId: IUIRepresentationNumberId as Record<any, TFieldScheme>,
  IUIRepresentationStringId: IUIRepresentationStringId as Record<any, TFieldScheme>,
};

export const baseModels: Record<string, TFieldScheme> = {
  boolean: baseBoolean as TFieldScheme,
  number: baseNumber as TFieldScheme,
  string: baseString as TFieldScheme,
};
