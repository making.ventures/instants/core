import {EFiledTypes} from './types';

type TNarFieldCommon = {description?: string; required?: boolean};
type TNarFieldStringCommon = TNarFieldCommon & {
  format?: 'date-time';
  enum?: string[];
};

export type TNarObjectField = TNarFieldCommon & {type: EFiledTypes.Object};
export type TNarNumberField = TNarFieldCommon & {type: EFiledTypes.Number};
export type TNarBoolField = TNarFieldCommon & {type: EFiledTypes.Bool};
export type TNarArrayNumField = TNarFieldCommon & {
  type: EFiledTypes.ArrayNum;
  items: {type: 'number'};
};
export type TNarArrayStrField = TNarFieldCommon & {
  type: EFiledTypes.ArrayStr;
  items: {type: 'string'};
};
export type TNarStringField = TNarFieldStringCommon & {
  type: EFiledTypes.String;
};
export type TNarDateField = TNarFieldStringCommon & {
  type: EFiledTypes.Date;
  format: 'date-time';
};
export type TNarEnumField = TNarFieldStringCommon & {
  type: EFiledTypes.Enum;
  enum: string[];
};
export type TNarEntityNumLinkField = TNarFieldCommon & {
  type: EFiledTypes.EntityNumLink;
  idOf: string;
};
export type TNarEntityArrayNumLinkField = TNarFieldCommon & {
  type: EFiledTypes.EntityArrayNumLink;
  idOf: string;
}

export type TNarFieldScheme =
  | TNarObjectField
  | TNarNumberField
  | TNarArrayNumField
  | TNarArrayStrField
  | TNarStringField
  | TNarEnumField
  | TNarBoolField
  | TNarDateField
  | TNarEntityNumLinkField
  | TNarEntityArrayNumLinkField;

export type TNarEntityScheme<T> = Record<keyof T, TNarFieldScheme>;
