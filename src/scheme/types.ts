export enum EFiledTypes {
  Number = 'Number',
  String = 'String',
  Bool = 'Bool',
  Date = 'Date',
  Enum = 'Enum',
  Object = 'Object',
  ArrayNum = 'ArrayNum',
  ArrayStr = 'ArrayStr',
  EntityNumLink = 'EntityNumLink',
  EntityArrayNumLink = 'EntityArrayNumLink'
}

type TFieldCommon = {description?: string};
type TFieldStringCommon = TFieldCommon & {
  type: 'string';
  format?: 'date-time';
  enum?: string[];
};

export type TObjectField = TFieldCommon & {type: 'object'};
export type TNumberField = TFieldCommon & {type: 'number'};
export type TBoolField = TFieldCommon & {type: 'boolean'};
export type TArrayNumField = TFieldCommon & {type: 'array'; items: {type: 'number'}};
export type TArrayStrField = TFieldCommon & {type: 'array'; items: {type: 'string'}};
export type TStringField = TFieldStringCommon;
export type TDateField = TFieldStringCommon & {format: 'date-time'};
export type TEnumField = TFieldStringCommon & {enum: string[]};
export type TEntityNumLinkField = TNumberField & {idOf: string};
export type TEntityArrayNumLinkField = TArrayNumField & {idOf: string};

export type TFieldScheme =
  | TObjectField
  | TNumberField
  | TArrayNumField
  | TArrayStrField
  | TStringField
  | TEnumField
  | TBoolField
  | TDateField
  | TEntityNumLinkField
  | TEntityArrayNumLinkField;

export type TEntityScheme<T> = Record<keyof T, TFieldScheme>;
