import {
  EFiledTypes,
  TFieldScheme,
  TDateField,
  TEnumField,
  TEntityNumLinkField,
  TEntityArrayNumLinkField,
} from './types';
import {jstr} from '../utils';
import {TNarFieldScheme} from './narrovedTypes';

export const defineType = (fieldScheme: TFieldScheme): EFiledTypes => {
  switch (fieldScheme.type) {
  case 'number':
    if ('idOf' in fieldScheme) {
      return EFiledTypes.EntityNumLink;
    }

    return EFiledTypes.Number;
  case 'boolean':
    return EFiledTypes.Bool;
  case 'string':
    if (fieldScheme.format === 'date-time') {
      return EFiledTypes.Date;
    }

    if (fieldScheme.enum) {
      return EFiledTypes.Enum;
    }

    return EFiledTypes.String;
  case 'object':
    return EFiledTypes.Object;
  case 'array':
    if (fieldScheme.items.type === 'number') {
      if ('idOf' in fieldScheme) {
        return EFiledTypes.EntityArrayNumLink;
      } else {
        return EFiledTypes.ArrayNum;
      }
    } else {
      return EFiledTypes.ArrayStr;
    }

  default:
    throw new Error(`Unexpected type, fieldScheme: "${jstr(fieldScheme)}"`);
  }
};

export const narrowField = (raw: TFieldScheme): TNarFieldScheme => {
  const type = defineType(raw);

  switch (type) {
  case 'Number':
    return {...raw, required: true, type: EFiledTypes.Number};
  case 'String':
    return {...raw, required: true, type: EFiledTypes.String};
  case 'Bool':
    return {...raw, required: true, type: EFiledTypes.Bool};
  case 'Date':
    return {
      ...(raw as TDateField),
      required: true,
      type: EFiledTypes.Date,
    };
  case 'Enum':
    return {
      ...(raw as TEnumField),
      required: true,
      type: EFiledTypes.Enum,
    };
  case 'Object':
    return {...raw, required: true, type: EFiledTypes.Object};
  case 'EntityNumLink':
    return {
      ...(raw as TEntityNumLinkField),
      required: true,
      type: EFiledTypes.EntityNumLink,
    };
  case 'EntityArrayNumLink':
    return {
      ...(raw as TEntityArrayNumLinkField),
      required: true,
      type: EFiledTypes.EntityArrayNumLink,
    };
  default:
    throw new Error(`Unexpected type, fieldScheme: "${jstr(raw)}"`);
  }
};
