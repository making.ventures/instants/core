import {ECurrency} from './enum';

export interface IPrice {
  amount: number; // In cents
  currency: ECurrency;
}
