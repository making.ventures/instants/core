import {ECurrency} from './enum';

export type TMultyMoney = Partial<Record<ECurrency, number>>;
