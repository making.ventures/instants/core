export type RequiredId<T extends {id?: any}> = T & {
  id: Required<T>['id'];
};

export type OptionalId<T extends {id?: any}> = T & {
  id?: IdType<T>;
};

export type IdType<T extends {id?: any}> = T['id'] extends string | undefined
  ? string
  : number;

export interface IBaseEntity<T extends TId = number> {
  id?: T;
  active?: boolean;
}

export type TBaseEntity<T extends TId = number> = {
  active?: boolean;
  id?: T;
};

export type TId = number | string;

export interface IHaveId<T extends TId = number>
  extends RequiredId<TBaseEntity<T>> {}

export interface IUIRepresentation<T extends TId = number> {
  id: T;
  title: string;
  comment?: string;
  image?: string;
  active?: boolean;
}

export interface IUIRepresentationNumberId extends IUIRepresentation<number> {}

export interface IUIRepresentationStringId extends IUIRepresentation<string> {}

export interface IImplementMe {
  id: string;
  some: string;
}
