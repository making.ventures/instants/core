export interface IMethodHttpMetadata {
  path: IHttpPath | ((base: string, id?: string | number) => IHttpPath);
}

export interface IHttpPath {
  method: EHttpMethod;
  path: string;
}
export enum EHttpMethod {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
}

export interface IPathMetaHolder {
  getPaths(): IHttpPath[];
}

export const typeStringHttpMethod = (value: EHttpMethod): string => {
  switch (value) {
  case EHttpMethod.GET:
    return 'get';
  case EHttpMethod.POST:
    return 'post';
  case EHttpMethod.PUT:
    return 'put';
  case EHttpMethod.DELETE:
    return 'delete';
  default:
    throw new Error(`Value ${value} is not known`);
  }
};
