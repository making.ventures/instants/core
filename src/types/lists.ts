import {ECurrency} from './enum';

export interface IListRequest
  extends IPageRequest,
  ISearchable,
  IIdsRequest,
  ILangRequest { }

export type TListRequest<
  T = any
  > = IListRequest & IBaseSort<keyof T> & TEntityFilter<T>;

export type TEntityFilter<
  T = any
  > = Partial<Record<keyof T, any>>;

export interface ILangListRequest extends IListRequest, ILangRequest { }

export interface ILangRequest {
  lang?: string;
}

export interface IPageRequest {
  elementsOnPage?: number;
  page?: number;
}

export interface IIdsRequest {
  ids?: Array<number | string>;
}

export interface ISearchable {
  search?: string;
}

export interface IPageInfo {
  currentPage?: number;
  elementsOnPage?: number;
  pageCount?: number;
}

export interface IList<T> {
  list: T[];
  pageInfo?: IPageInfo;
}

export enum EOrderDirection {
  ASC = 'ASC',
  DESC = 'DESC',
}

export interface IBaseSort<T> {
  sortDirection?: EOrderDirection;
  sortField?: T;
}

export interface IRawBaseSort {
  sortDirection?: EOrderDirection;
  sortField?: string;
}

export enum EPaginationAction {
  Next = 'Next',
  Prev = 'Prev',
}

export interface IIdRequest {
  id: number;
}

export interface ICurrencyListRequest extends IListRequest {
  currency?: ECurrency;
}

export interface IOnlyActiveListRequest extends IListRequest {
  onlyActive?: boolean;
}

export interface IResult<T> {
  data: T;
  status: string;
}
