export type TLocalization = 'EN' | 'RU';

export interface IWithLang {
  lang: TLocalization;
}
