import {RequiredId} from './base';
import {IList, IListRequest} from './lists';

export type TGet<T> = (
  id: number,
  webmasterId?: number,
) => Promise<RequiredId<T> | null>;

export type TGetWithRequest<T, TRequest = any> = (
  id: number,
  request?: TRequest,
) => Promise<RequiredId<T> | null>;

export type TGetStr<T> = (
  id: string,
  webmasterId?: number,
) => Promise<RequiredId<T> | null>;

export type TGetRequest<TRequest, TGet> = (
  request: TRequest,
) => Promise<RequiredId<TGet> | null>;

export type TCreate<TCreate, TGet> = (
  entity: TCreate,
  fromUser?: boolean,
) => Promise<RequiredId<TGet>>;

export type TList<TForListRequest, TForList> = (
  request?: TForListRequest,
  fromUser?: boolean,
  userId?: number,
) => Promise<IList<RequiredId<TForList>>>;

export type TUpdate<TUpdate, TGet> = (
  entity: RequiredId<TUpdate>,
) => Promise<RequiredId<TGet>>;

export type TDelete = (id: number) => Promise<number>;

export type TDeleteStr = (id: string) => Promise<string>;

export type TMethod<TRequest, TResponse> = (
  requset: TRequest,
) => Promise<TResponse>;

export enum EUiCrudMethods {
  UiCreate = 'uiCreate',
  UiGet = 'uiGet',
  UiList = 'uiList',
  UiUpdate = 'uiUpdate',
}

export enum ECrudMethods {
  Create = 'create',
  Delete = 'delete',
  Get = 'get',
  ListWithPageInfo = 'list',
  Update = 'update',
}

export enum EReprMethods {
  ReprGet = 'reprGet',
  ReprList = 'reprList',
}

export const uiCrudMethods = [
  EUiCrudMethods.UiCreate,
  EUiCrudMethods.UiGet,
  EUiCrudMethods.UiList,
  EUiCrudMethods.UiUpdate,
];

export const crudMethods = [
  ECrudMethods.Create,
  ECrudMethods.Delete,
  ECrudMethods.Get,
  ECrudMethods.ListWithPageInfo,
  ECrudMethods.Update,
];

export const reprMethods = [EReprMethods.ReprGet, EReprMethods.ReprList];

export type TAfterRead<T> = (entity: RequiredId<T>) => RequiredId<T>;

export type TUiListMethodWithPageInfo<
  TRequest extends IListRequest,
  TListEntity
  > = Record<EUiCrudMethods.UiList, TList<TRequest, TListEntity>>;

export type TUiUpdateMethod<TRequest, TResponse> = Record<
  EUiCrudMethods.UiUpdate,
  TUpdate<TRequest, TResponse>
>;

export type TUiCreateMethod<TRequest, TResponse> = Record<
  EUiCrudMethods.UiCreate,
  TCreate<TRequest, TResponse>
>;

export type TUiDeleteMethod = Record<ECrudMethods.Delete, TDelete>;

export type TUiDeleteStrMethod = Record<ECrudMethods.Delete, TDeleteStr>;

export type TUiGetMethod<TEntity> = Record<EUiCrudMethods.UiGet, TGet<TEntity>>;

export type TUiGetStrMethod<TEntity> = Record<
  EUiCrudMethods.UiGet,
  TGetStr<TEntity>
>;

export type TUiGetRequestMethod<TRequest, TEntity> = Record<
  EUiCrudMethods.UiGet,
  TGetRequest<TRequest, TEntity>
>;

export type TUpdateMethod<TRequest, TResponse> = Record<
  ECrudMethods.Update,
  TUpdate<TRequest, TResponse>
>;

export type TCreateMethod<TRequest, TResponse> = Record<
  ECrudMethods.Create,
  TCreate<TRequest, TResponse>
>;

export type TGetMethod<TEntity> = Record<ECrudMethods.Get, TGet<TEntity>>;
export type TDeleteMethod = Record<ECrudMethods.Delete, TDelete>;
export type TDeleteStrMethod = Record<ECrudMethods.Delete, TDeleteStr>;
