import {ECurrency} from '../enum';

export interface ICurrency {
  id: string;
  title: string;
  sign: string;
}

export type TCurrencies = Record<ECurrency, ICurrency>;
