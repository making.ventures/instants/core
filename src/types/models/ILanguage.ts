import {IBaseEntity} from '../base';

export interface ILanguage extends IBaseEntity<string> {
  name: string;
}
