import {IPageInfo, IPageRequest, IBaseSort} from './lists';
import {IWithLang} from './localization';

export interface IReportFilter extends IPageRequest {
  from: Date;
  to: Date;
}

export interface IReportRequest<Row>
  extends IBaseSort<keyof Row>,
    IPageRequest,
    IReportFilter {
  timeOffset?: number;
}

export type TServerResult<T> = {data: T}

export interface IReportRequestWithLang<Row>
  extends IReportRequest<Row>, IWithLang {}

export interface IReport<R, Request> {
  calculate(request: Request): Promise<IReportResult<R>>;
}

export interface IReportWithDownloadLink<R, Request> extends IReport<R, Request>{
  getDownloadLink(request: Request): Promise<TServerResult<string>>;
}

export interface IReportWithTotal<R, Request> {
  calculate(request: Request): Promise<IReportResultWithTotal<R>>;
}

export interface IReportWithTotalAndDownloadLink<R, Request> extends IReportWithTotal<R, Request>{
  getDownloadLink(request: Request): Promise<TServerResult<string>>;
}

export interface IReportResult<Row> {
  rows: Row[];
  pageInfo?: IPageInfo;
}

export interface IReportResultWithTotal<Row> extends IReportResult<Row> {
  total?: Partial<Row>;
}
