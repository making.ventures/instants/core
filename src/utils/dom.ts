import {parse} from 'querystring';

export function isMobile() {
  return window.innerWidth < 768;
}

export function getQuery() {
  return parse(window.location.search.slice(1));
}

export function getQueryStr() {
  const search = window.location.search;

  return search.length > 0 && search[0] === '?' ? search.slice(1) : search;
}
