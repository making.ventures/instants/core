export const jstr: (obj: any) => string = (object) =>
  JSON.stringify(object, undefined, ' ');

export const getTimeOffset = () => -1 * new Date().getTimezoneOffset();
